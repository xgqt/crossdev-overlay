#!/usr/bin/env bash


# This file is part of crossdev-overlay.

# crossdev-overlay is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# crossdev-overlay is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with crossdev-overlay.  If not, see <https://www.gnu.org/licenses/>.

# Original author: Maciej Barć (xgqt@riseup.net)
# Licensed under the GNU GPL v2 Licens


trap 'exit 128' INT
export PATH


FEATURES="-test"
export FEATURES

USE="-test"
export USE


main() {
    if ! which crossdev
    then
        emerge --verbose crossdev
    fi

    crossdev --stable --stage4 "${*}"
}


if [ "$(whoami)" != root ]
then
    echo "Switching to the root user account"
    sudo "${0}" "${*}"
else
    main "${*}"
fi
